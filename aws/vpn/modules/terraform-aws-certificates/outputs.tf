output "acm_cert_server_arn" {
  description = "The ARN of the server certificate"
  value       = aws_acm_certificate.server.arn
}

output "acm_cert_root_arn" {
  description = "The ARN of the client certificate"
  value       = aws_acm_certificate.root.arn
}

output "vpn_client_cert" {
  value = local_file.acm_client_cert.filename
}

output "vpn_client_key" {
  value = local_file.acm_client_key.filename
}
