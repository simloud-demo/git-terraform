variable "domain_name" {
  description = "A domain name for which the certificate should be issued"
  type        = string
  default     = ""
}

variable "organization_name" {
  description = "A domain name for which the certificate should be issued"
  type        = string
  default     = ""
}
