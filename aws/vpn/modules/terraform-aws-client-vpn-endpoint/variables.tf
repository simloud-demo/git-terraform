#variable "aws_access_key" {}

#variable "aws_secret_key" {}

variable "aws_region" {
  default = "eu-central-1"
}

variable "aws_client_cidr_block" {
  description = "The IPv4 address range, in CIDR notation being /22 or greater, from which to assign client IP addresses"
  default = "18.0.0.0/22"
}

variable "aws_target_cidr_block" {
  description = "The IPv4 address range, in CIDR notation being /22 or greater, from which to assign client IP addresses"
  default = "0.0.0.0/0"
}

variable "aws_create_destination_cidr_block" {
  description = "The IPv4 address range, in CIDR notation being /22 or greater, from which to assign client IP addresses"
  default = false
}

variable "aws_destination_cidr_block" {
  description = "The IPv4 address range, in CIDR notation being /22 or greater, from which to assign client IP addresses"
  default = "0.0.0.0/0"
}

variable "aws_subnet_id" {
  description = "The ID of the subnet to associate with the Client VPN endpoint."
}

variable "aws_cert_dir" {
  default = "certs"
}

variable "aws_domain" {
  default = "example.net"
}

variable "aws_srn" {}

variable "aws_vpn_name" {}
variable "aws_vpc_id" {}

variable "aws_vpn_split_tunnel" {
  default     = true
  description = "With split_tunnel false, all client traffic will go through the VPN."
}

variable "aws_acm_cert_client_arn" {}
variable "aws_acm_cert_server_arn" {}

variable "aws_vpn_client_key" {}
variable "aws_vpn_client_cert" {}
