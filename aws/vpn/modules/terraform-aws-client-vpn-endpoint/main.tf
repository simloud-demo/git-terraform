data "aws_vpc" "selected" {
  id = var.aws_vpc_id
}

resource "aws_ec2_client_vpn_endpoint" "client-vpn-endpoint" {
  description            = "terraform-clientvpn-endpoint"
  server_certificate_arn = var.aws_acm_cert_server_arn
  client_cidr_block      = var.aws_client_cidr_block
  split_tunnel           = var.aws_vpn_split_tunnel
  security_group_ids     = [aws_security_group.vpn_access.id]
  vpc_id                 = var.aws_vpc_id
  
  depends_on = [
    aws_security_group.vpn_access
  ]

  dns_servers = [
    cidrhost(data.aws_vpc.selected.cidr_block_associations[0].cidr_block, 2),
    cidrhost(data.aws_vpc.selected.cidr_block_associations[1].cidr_block, 2),
  ]

  authentication_options {
    type                       = "certificate-authentication"
    root_certificate_chain_arn = var.aws_acm_cert_client_arn
  }

  connection_log_options {
    enabled = false
  }

  tags = {
    Name = var.aws_vpn_name
  }
}

resource "aws_ec2_client_vpn_network_association" "client-vpn-network-association" {
  client_vpn_endpoint_id = aws_ec2_client_vpn_endpoint.client-vpn-endpoint.id
  subnet_id              = var.aws_subnet_id
}

resource "aws_ec2_client_vpn_authorization_rule" "authorize-client-vpn-ingress" {
  client_vpn_endpoint_id = aws_ec2_client_vpn_endpoint.client-vpn-endpoint.id
  target_network_cidr    = var.aws_target_cidr_block
  authorize_all_groups   = true

  depends_on = [
    aws_ec2_client_vpn_endpoint.client-vpn-endpoint,
    aws_ec2_client_vpn_network_association.client-vpn-network-association
  ]
}

resource "aws_ec2_client_vpn_route" "create-client-vpn-route" {
  count = var.aws_create_destination_cidr_block ? 1 : 0
  client_vpn_endpoint_id = aws_ec2_client_vpn_endpoint.client-vpn-endpoint.id
  destination_cidr_block = var.aws_destination_cidr_block
  target_vpc_subnet_id   = aws_ec2_client_vpn_network_association.client-vpn-network-association.subnet_id

  depends_on = [
    aws_ec2_client_vpn_endpoint.client-vpn-endpoint,
    aws_ec2_client_vpn_authorization_rule.authorize-client-vpn-ingress
  ]

}

resource "null_resource" "export-client-config" {
  provisioner "local-exec" {
    command = "aws --region ${var.aws_region} ec2 export-client-vpn-client-configuration --client-vpn-endpoint-id ${aws_ec2_client_vpn_endpoint.client-vpn-endpoint.id} --output text | sed 's/cvpn-endpoint/random.cvpn-endpoint/g' > vpn_config.ovpn"
  }

  depends_on = [
    aws_ec2_client_vpn_endpoint.client-vpn-endpoint,
    aws_ec2_client_vpn_authorization_rule.authorize-client-vpn-ingress,
    aws_ec2_client_vpn_route.create-client-vpn-route,
    aws_ec2_client_vpn_network_association.client-vpn-network-association,
  ]
}

resource "null_resource" "generate-client-config" {
  provisioner "local-exec" {
    command = <<-EOT
          sed -i '$ d' vpn_config.ovpn
          echo "<cert>" >> vpn_config.ovpn && cat ${var.aws_vpn_client_cert} >> vpn_config.ovpn && echo "</cert>" >> vpn_config.ovpn
          echo "<key>" >> vpn_config.ovpn && cat ${var.aws_vpn_client_key} >> vpn_config.ovpn && echo "</key>" >> vpn_config.ovpn
          echo "reneg-sec 0" >> vpn_config.ovpn
    EOT
  }

  depends_on = [
    null_resource.export-client-config,
  ]
}

resource "null_resource" "export-vpn-config-to-vault" {
  provisioner "local-exec" {
    command = "export VAULT_ADDR=http://localhost:8200 && vault kv put secrets/vpn/${var.aws_srn} ${aws_ec2_client_vpn_endpoint.client-vpn-endpoint.id}=@vpn_config.ovpn"
  }

  depends_on = [null_resource.generate-client-config]
}
