module "aws-certificates" {
  source  = "./modules/terraform-aws-certificates"

  domain_name       = var.simloud["dns"][0]["name"]
  organization_name = "Test Inc."

}

module "aws-client-vpn" {
  source = "./modules/terraform-aws-client-vpn-endpoint"

  aws_region                          = var.simloud["generic"]["region"]
  aws_vpc_id                          = var.simloud["vpc"]["id"]
  aws_subnet_id                       = var.simloud["vpc"]["subnets"]["internal"][0]
  aws_domain                          = var.simloud["dns"][0]["name"]
  aws_srn                             = var.simloud["generic"]["srn"]
  aws_vpn_name                        = var.simloud["eks"][0]["name"]
  aws_vpn_split_tunnel                = true
  aws_create_destination_cidr_block   = false
  aws_acm_cert_server_arn             = module.aws-certificates.acm_cert_server_arn
  aws_acm_cert_client_arn             = module.aws-certificates.acm_cert_root_arn
  aws_vpn_client_cert                 = module.aws-certificates.vpn_client_cert
  aws_vpn_client_key                  = module.aws-certificates.vpn_client_key
}
