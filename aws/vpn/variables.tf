#variable "access_key" {}
#variable "secret_key" {}

variable "aws_region" {
  default = "us-east-1"
}

variable "subnet_id" {
  default = "id"
}

variable "domain" {
  default = "example.com"
}

//variable "simloud" {
//  description = "Simloud Data"
//  type        = map(string)
//}

variable "simloud" {
  description = "Simloud Data"
  type        = any
}
