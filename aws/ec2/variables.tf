variable "instance_count" {
  type = number
  default = 1
}

variable "aws_region" {
  type        = string
  default     = "ap-south-1"  
}

variable "availability_zone" {
  type        = string
  default     = "ap-south-1a"  
}

variable "ami_id"{
  type        = string
  default     = "ami-0607784b46cbe5816"
}

variable "instance_type" {
  type = string
  default = "t3.micro"
}

variable "user_data" {
  type = string
  default = null
}

variable "user_data_base64" {
  type = string
  default = null
}

variable "key_name" {
  type = string
  default = null
}

variable "monitoring" {
  type = bool
  default = false
}

variable "get_password_data" {
  type = bool
  default = false
}

variable "vpc_security_group_ids" {
  type = list(string)
  default = []
}

variable "iam_instance_profile" {
  type = string
  default = null
}

variable "associate_public_ip_address" {
  type = bool
  default = true
}

variable "private_ips" {
  type = list(string)
  default = []
}

variable "ipv6_address_count" {
  type = number
  default = null
}

variable "ipv6_addresses" {
  type = list(string)
  default = null
}

variable "ebs_optimized" {
  type = bool
  default = false
}

variable "root_block_device" {
  type = list(map(string))
  default = [
    {
      "volume_type" = "gp2"
      "volume_size" = "8"
      "delete_on_termination" = "true"
    }
  ]
}

variable "ebs_block_device" {
  type = list(map(string))
  default = []
}

variable "ephemeral_block_device" {
  type = list(map(string))
  default = []
}

variable "network_interface" {
  type = list(map(string))
  default = []
}

variable "source_dest_check" {
  type = bool
  default = true
}

variable "disable_api_termination" {
  type = bool
  default = false
}

variable "instance_initiated_shutdown_behavior" {
  type = string
  default = "stop"
}

variable "placement_group" {
  type = string
  default = null
}

variable "tenancy" {
  type = string
  default = "default"
}

variable "name" {
  type = string
  default = "simloud_terraform_ec2_instance"
}

variable "use_num_suffix" {
  type = bool
  default = true
}

variable "num_suffix_format" {
  type = string
  default = "-%d"
}

variable "tags" {
  type = map(string)
  default = {}
}

variable "volume_tags" {
  type = map(string)
  default = {}
}

variable "cpu_credits" {
  type = string
  default = "standard"
}

variable "simloud" {
  description = "Simloud Data"
  type        = any
}

variable "sg_ingress_from_port" {
  default = 22
}

variable "sg_ingress_to_port" {
  default = 22
}

variable "sg_ingress_protocol" {
  default = "tcp"
}

variable "sg_ingress_cidr_blocks" {
  default = ["0.0.0.0/0"]
}

variable "sg_egress_from_port" {
  default = 0
}

variable "sg_egress_to_port" {
  default = 0
}

variable "sg_egress_protocol" {
  default = "-1"
}

variable "sg_egress_cidr_blocks" {
  default = ["0.0.0.0/0"]
}

variable "vpc_cidr" {
  default = "10.0.0.0/16"
  description = "VPC CIDR BLOCK"
  type = string
}

variable "public_s1" {
  default = "10.0.0.0/16"
  description = "public_subnet_1"
  type = string
}
