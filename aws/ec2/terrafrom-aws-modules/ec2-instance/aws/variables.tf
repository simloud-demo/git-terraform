variable "aws_subnet_id" {
  description = "The ID of the subnet to associate with the Client VPN endpoint."
}

variable "aws_cert_dir" {
  default = "certs"
}

variable "aws_domain" {
  default = "example.net"
}

variable "aws_srn" {}

resource "aws_security_group" {}