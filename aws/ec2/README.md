# git-terraform

This type of repository allows deploying custom configuration for terraform code from simloud-pipeline file.


### Terraform variables 
In the Terraform code, we use the following variables:

```TF
var.simloud["generic"]["region"]              = The AWS region name
var.simloud["generic"]["srn"]                 = The SRN of deployment      
var.simloud["vpc"]["id"]                      = The AWS VPC id.
var.simloud["vpc"]["subnets"]["eks"][0]       = The AWS EKS subnet id.
var.simloud["vpc"]["subnets"]["internal"][0]  = The AWS Internal subnet id.
var.simloud["vpc"]["subnets"]["public"][0]    = The AWS Public subnet id.
var.simloud["s3"]["name"]                     = The AWS S3 bucket name.  
var.simloud["dns"]["name"]                    = The AWS DNS customer domain.
var.simloud["dns"]["zoneid"]                  = The AWS DNS zone id.
var.simloud["dns"]["private"]                 = The AWS DNS zone type. Private or Public.
var.simloud["eks"]["name"]                    = The AWS EKS cluster name.
```
For more information, please, use documentation <a href="https://docs.simloud.com/en/simloud-pipeline.yaml/" target="_blank">Simloud-pipeline.yaml</a>.

Examples of simloud-pipeline.yaml files are available by this link <a href="https://docs.simloud.com/en/simloud-pipeline-examples/" target="_blank">Simloud-pipeline.yaml examples</a>.

