module "ec2_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
}

locals {
  is_t_instance_type = replace(var.instance_type, "/^t(2|3|3a){1}\\..*$/", "1") == "1" ? true : false
}

# terraform aws create vpc
resource "aws_vpc" "simloud-default" {
  cidr_block              = var.vpc_cidr
  instance_tenancy        = "default"
  enable_dns_hostnames    = true

  tags = {
    Name = "simloud-default"
  }
}

# terraform aws create internet gateway
resource "aws_internet_gateway" "simloud_terraform_internet_gateway" {
  vpc_id    = aws_vpc.simloud-default.id

  tags = {
    Name = "simloud_terraform_internet_gateway"
  }
}

# terraform aws create subnet
resource "aws_subnet" "simloud_terraform_public_s1" {
  vpc_id                  = aws_vpc.simloud-default.id
  cidr_block              = var.public_s1
  availability_zone       = var.availability_zone
  map_public_ip_on_launch = true

  tags = {
    Name = "simloud_terraform_public_subnet_1"
  }
}

# terraform aws create route table
resource "aws_route_table" "simloud_terraform_public_rt" {
  vpc_id       = aws_vpc.simloud-default.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.simloud_terraform_internet_gateway.id
  }

  tags = {
    Name = "simloud_terraform_public_routing_table"
  }
}

# terraform aws associate subnet with route table
resource "aws_route_table_association" "simloud_terraform_public_s1_rta" {
  subnet_id           = aws_subnet.simloud_terraform_public_s1.id
  route_table_id      = aws_route_table.simloud_terraform_public_rt.id
}

resource "aws_security_group" "simloud_terraform_ec2_sg1" {
  name        = "simloud_terraform_ec2_sg1"
  description = "Security Group SSH"
  vpc_id      = aws_vpc.simloud-default.id
  
  ingress {
    from_port   = var.sg_ingress_from_port
    to_port     = var.sg_ingress_to_port
    protocol    = var.sg_ingress_protocol
    cidr_blocks = var.sg_ingress_cidr_blocks
  }

  egress {
    from_port   = var.sg_egress_from_port
    to_port     = var.sg_egress_to_port
    protocol    = var.sg_egress_protocol
    cidr_blocks = var.sg_egress_cidr_blocks
  }

  tags = {
    Name = "simloud_terraform_ec2_sg1"
  }
}

data "aws_region" "current" {
  name = var.aws_region
}

resource "aws_instance" "simloud_terraform_ec2_instance" {
  count             = var.instance_count
  ami               = var.ami_id

  instance_type    = var.instance_type
  user_data        = var.user_data
  user_data_base64 = var.user_data_base64

  key_name               = var.key_name
  monitoring             = var.monitoring
  get_password_data      = var.get_password_data
  iam_instance_profile   = var.iam_instance_profile
  vpc_security_group_ids = var.vpc_security_group_ids

  subnet_id                   = aws_subnet.simloud_terraform_public_s1.id

  associate_public_ip_address = var.associate_public_ip_address 
  private_ip                  = length(var.private_ips) > 0 ? element(var.private_ips, count.index) : null
  ipv6_address_count          = var.ipv6_address_count
  ipv6_addresses              = var.ipv6_addresses

  security_groups = [
    aws_security_group.simloud_terraform_ec2_sg1.id 
  ]

  tags = {
    Name = "simloud_terraform_ec2_instance"
  }

  ebs_optimized = var.ebs_optimized

  dynamic "root_block_device" {
    for_each = var.root_block_device
    content {
      delete_on_termination = lookup(root_block_device.value, "delete_on_termination", null)
      encrypted             = lookup(root_block_device.value, "encrypted", null)
      iops                  = lookup(root_block_device.value, "iops", null)
      kms_key_id            = lookup(root_block_device.value, "kms_key_id", null)
      volume_size           = lookup(root_block_device.value, "volume_size", null)
      volume_type           = lookup(root_block_device.value, "volume_type", null)
    }
  }

  dynamic "ebs_block_device" {
    for_each = var.ebs_block_device
    content {
      delete_on_termination = lookup(ebs_block_device.value, "delete_on_termination", null)
      device_name           = ebs_block_device.value.device_name
      encrypted             = lookup(ebs_block_device.value, "encrypted", null)
      iops                  = lookup(ebs_block_device.value, "iops", null)
      kms_key_id            = lookup(ebs_block_device.value, "kms_key_id", null)
      snapshot_id           = lookup(ebs_block_device.value, "snapshot_id", null)
      volume_size           = lookup(ebs_block_device.value, "volume_size", null)
      volume_type           = lookup(ebs_block_device.value, "volume_type", null)
    }
  }

  dynamic "ephemeral_block_device" {
    for_each = var.ephemeral_block_device
    content {
      device_name  = ephemeral_block_device.value.device_name
      no_device    = lookup(ephemeral_block_device.value, "no_device", null)
      virtual_name = lookup(ephemeral_block_device.value, "virtual_name", null)
    }
  }

  dynamic "network_interface" {
    for_each = var.network_interface
    content {
      device_index          = network_interface.value.device_index
      network_interface_id  = lookup(network_interface.value, "network_interface_id", null)
      delete_on_termination = lookup(network_interface.value, "delete_on_termination", false)
    }
  }

  source_dest_check                    = length(var.network_interface) > 0 ? null : var.source_dest_check
  disable_api_termination              = var.disable_api_termination
  instance_initiated_shutdown_behavior = var.instance_initiated_shutdown_behavior
  placement_group                      = var.placement_group
  tenancy                              = var.tenancy

  # tags = merge(
  #   {
  #     "Name" = var.instance_count > 1 || var.use_num_suffix ? format("%s${var.num_suffix_format}", var.name, count.index + 1) : var.name
  #   },
  #   var.tags,
  # )

  volume_tags = merge(
    {
      "Name" = var.instance_count > 1 || var.use_num_suffix ? format("%s${var.num_suffix_format}", var.name, count.index + 1) : var.name
    },
    var.volume_tags,
  )

  credit_specification {
    cpu_credits = local.is_t_instance_type ? var.cpu_credits : null
  }
}